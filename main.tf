data "ignition_file" "prometheus_config" {
  path = "${var.home}/prometheus.yml"
  content {
    content = templatefile("${path.module}/templates/prometheus.yml", {
      federated_servers = var.federated_servers
      static_jobs = var.static_jobs
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "volume_unit" {
  path = "${var.home}/.config/containers/systemd/prometheus.volume"
  content {
    content = file("${path.module}/templates/prometheus.volume")
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "container_unit" {
  path = "${var.home}/.config/containers/systemd/prometheus.container"
  content {
    content = templatefile("${path.module}/templates/prometheus.container", {
      publish_host = var.publish_host
      home = var.home
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "config" {
  files = [
    data.ignition_file.prometheus_config.rendered,
    data.ignition_file.volume_unit.rendered,
    data.ignition_file.container_unit.rendered,
  ]
}
