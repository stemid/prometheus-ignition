variable "home" {
  type = string
}

variable "uid" {
  type = number
}

variable "gid" {
  type = number
}

variable "publish_host" {
  type = string
  default = "127.0.0.1"
}

variable "federated_servers" {
  type = list(string)
  default = []
}

variable "static_jobs" {
  type = list(object({
    name = string
    addresses = list(string)
  }))
  default = []
}
